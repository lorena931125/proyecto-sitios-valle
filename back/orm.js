const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;

var dbM = "mongoTourism";
var urlMongo = "mongodb://localhost:27017/";


module.exports.findSiteById = function(resserver, idSite) {
    MongoClient.connect(urlMongo, (err, database) => {
        var dbo = database.db(dbM);

        dbo.collection("sites").find({"_id": new ObjectId(idSite)}).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            resserver.send(result);
            database.close();
        });
    });
};

module.exports.findSiteByType = function(resserver, typeSite) {
    MongoClient.connect(urlMongo, (err, database) => {
        var dbo = database.db(dbM);

        dbo.collection("sites").find({"type": typeSite}).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            resserver.send(result);
            database.close();
        });
    });
};

module.exports.findSite = function(resserver) {
    MongoClient.connect(urlMongo, (err, database) => {
        var dbo = database.db(dbM);

        dbo.collection("sites").find({}).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            resserver.send(result);
            database.close();
        });
    });
};

module.exports.findSiteByQuery = function(resserver, queryInput) {
    MongoClient.connect(urlMongo, (err, database) => {
        var dbo = database.db(dbM);

        

        dbo.collection("sites").find({
            $or: [ 
                { category: {$regex: queryInput, $options: "i"}}, 
                { type:     {$regex: queryInput, $options: "i"}}, 
                { name:     {$regex: queryInput, $options: "i"}}, 
                { city:     {$regex: queryInput, $options: "i"}}
            ]
        }).toArray(function(err, result) {
            if (err) throw err;
            console.log(result);
            resserver.send(result);
            database.close();
        });
    });
};


module.exports.saveSite = function(myobj, resserver) {
    MongoClient.connect(urlMongo, (err, database) => {
        var dbo = database.db(dbM);
        dbo.collection("sites").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            resserver.send("Registro ingresado");
            database.close();
        });
    });
};

module.exports.updateSite = function(myobj, id, resserver) {
    MongoClient.connect(urlMongo, function(err, db) {
        if (err) throw err;
        var dbo = db.db(dbM);
        var myquery = { _id: ObjectId(id) };
        var nobject={ $set: myobj }
        dbo.collection("sites").updateOne(myquery, nobject, { upsert: true },function(err, res) {
            if (err) throw err;
            resserver.send("Registro Actualizado");
            db.close();
        });
    });
};