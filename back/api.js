var express = require('express');
var app = express();
var orm = require('./orm');
var bodyParser = require('body-parser');
//zzzzapp.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
}); 

app.route('/sites').get(function(req, res) {
    orm.findSite(res);

});


app.route('/sites/:idSite').get(function(req, res) {
    var idSite = req.params.idSite;
    orm.findSiteById(res, idSite);
});

app.route('/sites/type/:typeSite').get(function(req, res) {
    var typeSite = req.params.typeSite;
    orm.findSiteByType(res, typeSite);
});

app.route('/sites/search/:query').get(function(req, res) {
    var query = req.params.query;
    orm.findSiteByQuery(res, query);
});


app.route('/sites/save').post(function(req, res) {

    console.log(req.body);
    orm.saveSite(req.body, res);


});

app.route('/sites/update/:idSite').put(function(req, res) {
    var idSite = req.params.idSite;
    console.log(req.body);
    orm.updateSite(req.body, idSite,res);


});


var server = app.listen(8080, function() {});