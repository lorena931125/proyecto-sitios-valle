import { Component, OnInit } from '@angular/core';
import {SitesService} from '../services/sites.service';
import {SiteModel} from '../site-model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  sites:Array<SiteModel>=null;
  constructor(private api: SitesService) { }

  ngOnInit() {
  this.getAllSites();

  }

  getAllSites(){
    console.log('ssf')
    this.api.getAllSites().subscribe( (data:any) => {
      console.log(data)
     this.sites=data;
    });
  }

}
