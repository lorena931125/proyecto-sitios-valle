import { Component, OnInit } from '@angular/core';
import { SitesService } from '../services/sites.service';

@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css']
})
export class SiteComponent implements OnInit {

  public arraySites: any = [];

  constructor(private api: SitesService) { }

  ngOnInit() {
    
    this.getAllSites();
  }

  getAllSites(){
    this.api.getAllSites().subscribe( (data:any) => {
      this.arraySites = [];
      for (const d of (data as any)) {
        this.arraySites.push({
          id:     d._id,
          name:     d.name,
          type:     d.type,
          category: d.category,
          price:    d.price,
          address:  d.address,
          images:   d.images
        });
      }
      
    });
  }

}
