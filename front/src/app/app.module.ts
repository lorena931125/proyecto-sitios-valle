import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { SiteComponent } from './site/site.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdministratorComponent } from './administrator/administrator.component';
import { SitesService } from './services/sites.service';

import { AdminComponent } from './admin/admin.component';
import { AdminSiteComponent } from './admin-site/admin-site.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SiteDetailComponent } from './site-detail/site-detail.component';
import { ModalLoadingComponent } from './modal-loading/modal-loading.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    SiteComponent,
    NotFoundComponent,
    AdministratorComponent,
    AdminComponent,
    AdminSiteComponent,
    SiteDetailComponent,
    ModalLoadingComponent,
    SearchComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   HttpClientModule,
   FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    SitesService

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
