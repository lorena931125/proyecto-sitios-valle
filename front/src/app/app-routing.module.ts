import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SiteComponent } from './site/site.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AdministratorComponent } from './administrator/administrator.component';
import {AdminSiteComponent} from './admin-site/admin-site.component';
import {AdminComponent} from './admin/admin.component';
import { SiteDetailComponent } from './site-detail/site-detail.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [
  {
    path: 'site',
    component: SiteComponent
  },
  {
    path: 'sitedetail/:id',
    component: SiteDetailComponent
  },
  {
    path: 'search/:query',
    component: SearchComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'administrator',
    component: AdministratorComponent
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  { path: 'adminCreate', component: AdminSiteComponent },
  { path: 'admin', component: AdminComponent },
  { path: "adminCreate/:id", component: AdminSiteComponent },
  {
    path: '**',
    redirectTo: 'not-found'
  }];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
