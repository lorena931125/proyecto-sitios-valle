import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SiteModel } from '../site-model';
import { SitesService } from '../services/sites.service';
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-admin-site',
  templateUrl: './admin-site.component.html',
  styleUrls: ['./admin-site.component.css']
})
export class AdminSiteComponent implements OnInit {
  ciudades = [
    "Alcal\u00e1",
    "Andaluc\u00eda",
    "Ansermanuevo",
    "Argelia",
    "Bol\u00edvar",
    "Buenaventura",
    "Buga",
    "Bugalagrande",
    "Caicedonia",
    "Cali",
    "Calima",
    "Candelaria",
    "Cartago",
    "Dagua",
    "El \u00c1guila",
    "El Cairo",
    "El Cerrito",
    "El Dovio",
    "Florida",
    "Ginebra",
    "Guacar\u00ed",
    "Jamund\u00ed",
    "La Cumbre",
    "La Uni\u00f3n",
    "La Victoria",
    "Obando",
    "Palmira",
    "Pradera",
    "Restrepo",
    "Riofr\u00edo",
    "Roldanillo",
    "San Pedro",
    "Sevilla",
    "Toro",
    "Trujillo",
    "Tulu\u00e1",
    "Ulloa",
    "Versalles",
    "Vijes",
    "Yotoco",
    "Yumbo",
    "Zarzal"
  ];
  nameFiles: String = "Escoger Archivo";
  siteGroup: FormGroup;
  submitted = false;
  siteModel: SiteModel;
  fileSite: Array<string> = [];
  imgURL: any;
  fileError: String;
  saveform: boolean = false;
  idSite:String="";
  estadoS:String="";
  constructor(private formBuilder: FormBuilder, private api: SitesService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.siteModel = new SiteModel();
    this.siteGroup = this.formBuilder.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
      price: ['', Validators.required],
      type: ['', Validators.required],
      ciudad: ['', Validators.required],
      state: ['', Validators.required],
      fevento: ['',null],
      category: ['', Validators.required]

    });
    this.route.paramMap.subscribe(params => {
      var id = params.get("id");
      this.idSite=id;
      if (id != undefined) {
        this.api.getSiteById(id).subscribe((data: SiteModel) => {
          this.f.name.setValue(data[0].name);
          this.f.type.setValue(data[0].type);
          console.log(this.f.type.value);
          this.f.category.setValue(data[0].category);
          this.f.address.setValue(data[0].address);
          this.f.price.setValue(data[0].price);
          this.fileSite=data[0].images;
          this.imgURL = data[0].images[0];
          this.f.ciudad.setValue(data[0].city);
          this.f.state.setValue(data[0].state);
          console.log(data[0].dateEvent)
        if (data[0].dateEvent != undefined) {
             this.f.fevento.setValue(data[0].dateEvent);
      }

         
        })
      }
    })
  }


  get f() { return this.siteGroup.controls; }
  fileEvent(fileInput) {
    const target = event.target as HTMLInputElement;
    let fs: Array<any> = [];
    const files = (target.files)
    let filesname = "";

    for (var i = 0; i < files.length; i++) {
      filesname = this.loadFiles(files[i], filesname);
    }

    console.log(filesname);
    this.nameFiles = filesname;

  }

  loadFiles(file, filesname) {

    filesname += file.name + ",";
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (e) => {
      //me.modelvalue = reader.result;
      var img = reader.result;
      this.imgURL = img;
      console.log(img + "")
      this.fileSite.push(img + "");


    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
    return filesname;
  }

  setType() {
    console.log(this.f.type.value);

  }


  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    console.log(this.f)
    console.log(this.siteGroup.invalid);
    if (this.siteGroup.invalid) {
      return;
    }
    if (this.fileSite.length > 0) {
      this.fileError = "";
      this.siteModel.name = this.f.name.value;
      this.siteModel.category = this.f.category.value;
      this.siteModel.address = this.f.address.value;
      this.siteModel.price = this.f.price.value;
      this.siteModel.images = this.fileSite;
      this.siteModel.type = this.f.type.value;
      this.siteModel.city = this.f.ciudad.value;
      this.siteModel.state= this.f.state.value;
      if (this.f.fevento != undefined) {
        this.siteModel.dateEvent = this.f.fevento.value;
      }
      console.log(this.siteModel);
      if(this.idSite != ""){
        this.api.updateSite(this.siteModel,this.idSite).subscribe();
        this.estadoS="actualizado";
        this.saveform = true;
      }else{
        this.api.saveSite(this.siteModel).subscribe();
        this.saveform = true;
        this.estadoS="guardado";
      } 
    
     
    } else {
      this.saveform = false;
      this.fileError = ' se debe ingresar una imagen';

    }
  }
}
