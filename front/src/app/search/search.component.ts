import { Component, OnInit } from '@angular/core';
import { SitesService } from '../services/sites.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';


@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public arraySites: any = [];
  public query:any;

  constructor(private route: ActivatedRoute, private api: SitesService, private location: Location, private router2: Router) {
    router2.events.subscribe((val) => {
      if(val instanceof NavigationEnd){
        this.query = this.route.snapshot.params.query;
        if(this.query !== undefined && this.query !== null && this.query !== ""){
          this.findSiteByQuery(this.query);
        }
      }
    });
  }

  ngOnInit() {
    this.query = this.route.snapshot.params.query;
    if(this.query !== undefined && this.query !== null && this.query !== ""){
      this.findSiteByQuery(this.query);
    }
  }

  findSiteByQuery(query:any){
    this.location.replaceState('/search/'+query);

    console.log("Bucando por: "+query);
    this.api.getSiteByQuery(query).subscribe( (data:any) => {
      this.arraySites = [];
      for (const d of (data as any)) {
        this.arraySites.push({
          id:     d._id,
          name:     d.name,
          type:     d.type,
          category: d.category,
          price:    d.price,
          address:  d.address,
          images:   d.images
        });
      }
      console.log(this.arraySites);
    });
  }

}
