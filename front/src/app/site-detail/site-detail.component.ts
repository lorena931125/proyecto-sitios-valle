import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SitesService } from '../services/sites.service';

@Component({
  selector: 'app-site-detail',
  templateUrl: './site-detail.component.html',
  styleUrls: ['./site-detail.component.css']
})
export class SiteDetailComponent implements OnInit {

  public siteSelected:any = {};
  public arraySites: any = [];
  public showModal:boolean = true;

  constructor(private route: ActivatedRoute, private api: SitesService) { }

  ngOnInit() {
    
    let idSite = this.route.snapshot.params.id;
    if(idSite !== undefined && idSite !== null && idSite !== ""){
      this.api.getSiteById(idSite).subscribe( (data:any) => {
        this.siteSelected = data[0];

        this.api.getSiteByType(this.siteSelected.type).subscribe( (dataType:any) => {
          this.arraySites = [];
          for (const d of (dataType as any)) {
            this.arraySites.push({
              id:     d._id,
              name:     d.name,
              type:     d.type,
              category: d.category,
              price:    d.price,
              address:  d.address,
              images:   d.images
            });
          }
          
        });

      });
    }
  }

}
