import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router, private location: Location) {}
  title = 'front';
  findSiteByQuery(query:any){
    console.log("Debe ir a buscar: "+query);
    this.location.go("search/"+query);
    this.router.navigate(['search/'+query ]).then(nav => {
      console.log(nav); // true if navigation is successful
    }, err => {
      console.log(err) // when there's an error
    });
  }
}
