import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import {SiteModel} from '../site-model';

@Injectable({
  providedIn: 'root'
})
export class SitesService {

  public url:string;

  constructor(private http : HttpClient){
    this.url = environment.urlServices;
  }

  getAllSites(){
    return this.http.get(this.url+"sites");
  }

  saveSite(siteModel:SiteModel){

    return this.http.post(this.url+"sites/save",siteModel);
  }

  updateSite(siteModel:SiteModel,id){

    return this.http.put(this.url+"sites/update/"+id,siteModel);
  }

  getSiteById(idSite:any){
    let res = this.http.get(this.url+"sites/"+idSite);
    return res;
  }

  getSiteByType(typeSite:any){
    let res = this.http.get(this.url+"sites/type/"+typeSite);
    return res;
  }

  getSiteByQuery(query:any){
    let res = this.http.get(this.url+"sites/search/"+query);
    return res;
  }
}
